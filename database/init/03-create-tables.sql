-- For assignment 2020-02-14

USE cse451;

CREATE TABLE keyValue (
    pk INT(11) NOT NULL AUTO_INCREMENT,
    keyName TEXT NOT NULL,
    value TEXT NOT NULL,
    timestamp TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (pk)
);

INSERT INTO keyValue (keyName, value)
VALUES
    ('test1', 'hello'),
    ('test2', 'hello2'),
    ('Miami', 'Computer Science'),
    ('Quadratic', 'ax^2+b^x=c'),
    ('c45443', '3ff3ffde')
