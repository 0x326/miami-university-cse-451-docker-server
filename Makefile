EDITOR ?= vi

EXAMPLE_FILES := $(wildcard *.example) $(wildcard **/*.example) $(wildcard **/**/*.example)
SECRET_FILES := $(patsubst %.example,%,$(EXAMPLE_FILES))

up: $(SECRET_FILES)
	docker-compose up --detach

down:
	docker-compose down

secrets: $(SECRET_FILES)

web/public:
	-mkdir web/
	-mkdir web/public

database/init:
	-mkdir database
	-mkdir database/init

database/init/02-add-users.secret.sql: USER := meyerjm
database/init/02-add-users.secret.sql: DB := cse451
database/init/02-add-users.secret.sql: database/init/02-add-users.secret.sql.example database/init
	@echo "Generating random credentials for user $(USER) with access to db $(DB)"
	@cat $< | sed 's/password/$(shell openssl rand -base64 32)/g' | sed 's/username/$(USER)/g' | sed 's/db/$(DB)/g' > $@

.PHONY: up down secrets
